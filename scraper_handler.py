from bs4 import BeautifulSoup
import requests
import datetime



class Wordoftheday:
    def __init__(self, set_date = None):
        if set_date:
            date_str = set_date.strftime('%Y-%m-%d')
        else:
            date_str = ''

        self.URL = 'https://www.merriam-webster.com/word-of-the-day/' + date_str
        self._page = requests.get(self.URL).text # TODO: Fix 404
        self._soup = BeautifulSoup(self._page, 'html.parser')

        self.word = self._soup.find(class_='word-and-pronunciation').h1.text
        self.word_type = self._soup.find(class_='word-attributes').find(class_='main-attr').text
        self.syllables = self._soup.find(class_='word-attributes').find(class_='word-syllables').text

        self.definition = self.getDefinitions()
        self.didyouknow = self._soup.find(class_='did-you-know-wrapper').find(class_='left-content-box').p.text

        self.examples = [p.text for p in self._soup.find(class_='wotd-examples').find_all('p')]

    def getDefinitions(self):
        definitions = []
        for child in self._soup.find(class_='wod-definition-container').findChildren():
            if child.name == 'p':
                definitions += [child]
            if child.name == 'h2' and child.text == 'Did You Know?':
                break

        formatted_definitions = []
        for definition in definitions:
            split_definition = definition.text.split(':')[1:]
            formatted_definitions += [[definition_.strip() for definition_ in split_definition]]
        return formatted_definitions


today = Wordoftheday(datetime.date(2020,8,23))
print('\t'*3, today.word, '\n', '\t'*2, today.word_type, '|', today.syllables, '\n  ', today.URL)
#another = Wordoftheday(datetime.date(2020,8,20))
#print('\t', another.word, '\n', another.type, '|', another.syllables, '\n', another.definition)

#x = {
#    'word': today.word,
#    'word_type': today.word_type,
#    'syllables': today.syllables,
#    'definition': today.definition,
#    'didyouknow': today.didyouknow,
#    'examples': today.examples,
#    'URL': today.URL
#}
#import pickle
#with open('test.pickle', 'wb') as file:
#    pickle.dump(x, file)
