from rich.text import Text
from rich.console import Console
from rich.table import Table
from rich.padding import Padding

wotd = {'word': 'forte',
 'word_type': 'noun',
 'syllables': 'FOR-tay',
 'definition': [["one's strong point"],
  ['the part of a sword or\xa0foil\xa0blade that is between the middle and the\xa0hilt\xa0and that is the strongest part of the blade']],
 'didyouknow': 'Forte derives from the sport of fencing. When English speakers borrowed the word from French in the 17th century, it referred to the strongest part of a sword blade, between the middle and the hilt. It is therefore unsurprising that forte eventually developed an extended metaphorical sense for a person\'s strong point. (Incidentally, forte has its counterpoint in the word foible, meaning both the weakest part of a sword blade and a person\'s weak point.) There is some controversy over how to correctly pronounce forte. Common choices in American English are "FOR-tay" and "for-TAY," but many usage commentators recommend rhyming it with fort. In French, it would be written le fort and pronounced more similar to English for. You can take your choice, knowing that someone somewhere will dislike whichever variant you choose. All, however, are standard.',
 'examples': ['"Fried chicken is its forte, including spicy and boneless versions.… Its other specialty is breakfast…."',
  '"After looking through the gaming options, we decided on Quick Draw—a game that gives one participant a word to draw, while the other callers try to guess what the word is. … And while it turns out that guessing a word based on a sketch is not my forte (I got maybe one right), I was amazed at how mesmerized my whole family was.'],
 'URL': 'https://www.merriam-webster.com/word-of-the-day/2020-08-23'}

console = Console(width=60)

console.print()
console.print(f"[bold]{wotd['word']}", justify="center")
console.print(f"[light_green][i]{wotd['word_type']}[/]    [red]{wotd['syllables']}", justify="center")

grid = Table.grid()
grid.add_column()
grid.add_column()
grid.add_row("[blue bold]1[/]: ", f"{wotd['definition'][0][0]}")
grid.add_row("[blue bold]2[/]: ", f"{wotd['definition'][1][0]}")

console.print(Padding(grid, (1,2,0,2)))

#grid2 = Table.grid()
#grid2.add_column()
#grid2.add_row(f"{wotd['didyouknow']}")
#console.print(Padding(grid2, (1,2,0,2)))

grid3 = Table.grid(padding = (1,0))
grid3.add_column()
grid3.add_row(f"[i]{wotd['examples'][0]}")
grid3.add_row(f"[i]{wotd['examples'][1]}")

console.print(Padding(grid3, (1,2,0,2)))

grid4 = Table.grid(expand=True)
grid4.add_column()
grid4.add_column(justify="right")
grid4.add_row(f"[blue link {wotd['URL']}]More…[/]", f"August 23, 2020")

console.print(Padding(grid4, (1,2,1,2)))
